from flask import Flask, render_template, request
import urllib, json, subprocess, re, ast, requests, string, zipfile

app = Flask(__name__)

@app.route("/", methods=['POST', 'GET'])
def index():
    list_video = []
    # Homepage is movies
    if "search" not in request.form :
        res = ast.literal_eval(requests.post("https://yts.to/api/v2/list_movies.json?sort=seeds&limit=600&order_by=desc").text)
        for entry in res["data"]["movies"] :
            id = entry["id"]
            title = entry["title"]
            cover = string.replace(str(entry["medium_cover_image"]),"\\","")
            uri = string.replace(str(entry["torrents"][0]["url"]),"\\","")
            list_video.append((id, title, cover, uri))
        type_result=None
    if "search" in request.form :
        search = request.form["search"]
        provider = request.form["provider"]
        if provider == "movie" :
            type_result = "movies"
#            res = ast.literal_eval(requests.post("https://yts.to/api/v2/list_movies.json?sort=seeds&limit=600&order_by=desc?keywords=%s"%(search.replace(" ","+"))).text)
            res = ast.literal_eval(requests.post("https://yts.to/api/v2/list_movies.json?limit=50&sort_by=title&order_by=asc&query_term=%s"%(search.replace(" ","+"))).text)
            for entry in res["data"]["movies"] :
                id = entry["id"]
                title = entry["title"]
                cover = string.replace(str(entry["medium_cover_image"]),"\\","")
                uri = string.replace(str(entry["torrents"][0]["url"]),"\\","")
               # peers = entry["TorrentPeers"]
               # quality = entry["Quality"]
                list_video.append((id, title, cover, uri))
        elif provider == "shows" :
            type_result = "shows"
            res = requests.post("http://eztv.it/showlist/")
            res=re.compile("/shows/([^/]+)/([^/]+)").findall(res.text)
            search=search.split(" ")
            for (_id, title) in res :
                valid=True
                for s in search :
                    if s not in title :
                        valid = False; break
                if valid : list_video.append((_id, title))
        elif provider == "shows_" :
            type_result = "show"
            res = requests.post("http://eztv.it/shows/%s/"%(search))
            episodes=re.compile("/ep/[^/]+/([^/]+)").findall(res.text)
            magnets=re.compile('a href="(magnet[^"]+)"').findall(res.text)
            for i, episode in enumerate(episodes) :
                list_video.append((episode, magnets[i]))
    return render_template("index.html", list_video=list_video, type_result=type_result)

@app.route("/play", methods=['POST', 'GET'])
def play():
    url=request.args["url"]
    subt=request.args["subt"]
    adressOfSub = "http://www.yifysubtitles.com"+subt
    if subt != "None":
        subprocess.check_call(["mkdir /tmp/subtitle-api"], shell=True)
        urllib.urlretrieve (adressOfSub, "/tmp"+subt)
        zfile = zipfile.ZipFile("/tmp"+subt)
        for name in zfile.namelist():
          zfile.extract(name, "/tmp")
        subprocess.check_call(["peerflix '%s' -t /tmp/*.srt --mplayer&"%(url)], shell=True)
    else:
        subprocess.check_call(["peerflix '%s' --mplayer&"%(url)], shell=True)
    return ''

@app.route("/stop", methods=['POST', 'GET'])
def stop():
    print "stop"
    try : 
        subprocess.check_call(["pgrep 'youtube-dl|omxplayer.bin|mplayer|peerflix'| xargs kill"], shell=True)
        subprocess.check_call(["rm /tmp/*.srt"], shell=True)
    except : pass
    return ''

@app.route('/movie/<id>')
def show_movie(id):
    # get info of movie by API
    res = ast.literal_eval(requests.post("https://yts.to/api/v2/movie_details.json?movie_id="+id+"&with_images=true&with_cast=true").text)
    movie = res["data"]
    movie['images']['large_cover_image']=string.replace(str(movie['images']['large_cover_image']),"\\","")
    # remove \ to url torrents
    for t in movie['torrents']:
        t['url']=string.replace(str(t['url']),"\\","")

    #get subtitles
    list_lang=[("aucun",None)]
    res = json.loads(requests.post("http://api.yifysubtitles.com/subs/"+movie["imdb_code"]).text)
    subts = res["subs"][movie["imdb_code"]]
    for l in subts:
        list_lang.append((l, subts[l][0]['url']))
    return render_template("movie.html", movie=movie, list_lang=list_lang)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8001,debug=True)
