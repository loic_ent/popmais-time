Popmaïs Time
===============
Popmaïs is inspired of the famous [Popcorn Time](https://popcorntime.io).
But Popmaïs is implemented in Python. Popmaïs is technically simpler than Popcorn Time, so it's more portable and, I hope, easier to install. However, the content is identical.

***

<!-- ## Getting Involved
Want to report a bug, request a feature, contribute or translate Popcorn Time? Check out our in-depth guide to [Contributing to Popcorn Time](CONTRIBUTING.md). We need all the help we can get! You can also join in with our [community](README.md#community) to keep up-to-date and meet other Popcorn Timers. -->



## Difference with Popcorn Time
The major difference is that Popmaïs is a webapp, (run in any browser). Because Popcorn Time need many lib for GUI and that is the delicate point for the portability

## Getting Started

Popmaîs is developed on Unix system, so on Unix is very easy to run it.
If you're on windows, I did not test but I will.  Please, be patient or try yourself (install requirements and run commands in windows console like on Unix).

The [master](https://gitlab.com/loic_ent/popmais-time/tree/master) branch which contains the latest release.


#### Requirements:
-  `python2.7` : [https://www.python.org](https://www.python.org/downloads/)
-  `node` : [https://nodejs.org](https://nodejs.org)
-  `pip` : [get-pip.sh](https://pip.pypa.io/en/latest/installing.html)

#### Installation :

1. Download the [master](https://gitlab.com/loic_ent/popmais-time/repository/archive.zip?ref=master) branch

1. Uncompress the archive and go in

1. > npm install -g peerflix
1. > pip install Flask

#### Usage:
Start server :
> python index.py

And go to [http://localhost:8001](http://localhost:8001)




<!-- 
## Community

Keep track of Popcorn Time development and community activity.

* Follow Popcorn Time on [Twitter](https://twitter.com/popcorntimetv), [Facebook](https://www.facebook.com/PopcornTimeTv) and [Google+](https://plus.google.com/+Getpopcorntime).
* Read and subscribe to the [The Official Popcorn Time Blog](https://blog.popcorntime.io).
* Join in discussions on the [Popcorn Time Forum](https://discuss.popcorntime.io)
* Connect with us on IRC at `#popcorntime` on freenode ([web access](http://webchat.freenode.net/?channels=popcorntime))
 -->
<!-- 
## Versioning

For transparency and insight into our release cycle, and for striving to maintain backward compatibility, Popcorn Time will be maintained according to the [Semantic Versioning](http://semver.org/) guidelines as much as possible.

Releases will be numbered with the following format:

`<major>.<minor>.<patch>-<build>`

Constructed with the following guidelines:

* A new *major* release indicates a large change where backwards compatibility is broken.
* A new *minor* release indicates a normal change that maintains backwards compatibility.
* A new *patch* release indicates a bugfix or small change which does not affect compatibility.
* A new *build* release indicates this is a pre-release of the version.
 -->

***

If you distribute a copy or make a fork of the project, you have to credit this project as source.
    
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/ .

***

If you want to contact me : [Loïc](mailto:loic_ent@yahoo.fr)